# terraform-gitlab-vars

[RU version](docs/RU_README.md)

This project allows you to automate the process of addition `gitlab` variables that are necessary for operation `terraform` with `google cloud`.

For correct  work of `terraform` project need a file with `google application credentials` and `terraform.tfvars`. Both of these files forward with `terraform` to `CI/CD` variables of `gitlab` project.
The file with `google application credentials` deployed in encoded format for more secure transfer on the Internet.

---

## Usage

### 1. Getting google application credentials

```
gcloud auth application-default login
```

### 2. Setting up terraform.tfvars file

You need to fill in `terraform.tfvars` file with according to your settings

```
cp terraform.tfvars.example terraform.tfvars
```

### 3. Running
For running, you need to perform these commands

```
terraform init 
terraform plan
terraform apply -auto-approve 
```

### 4. Running for several projects

For setting up several `gitlab `projects, recommended to use `terraform workspace`

```
terraform init 
terraform workspace new <project-name>
terraform plan -var-file -var-file=<project-terraform-tfvars> 
terraform apply -auto-approve -var-file -var-file=<project-terraform-tfvars> 
```

### 5. Destroying
```
terraform destroy -auto-approve 
```

---

## Installation

you may install all of the tools with these guides 

* [Google Cloud sdk](https://cloud.google.com/sdk/docs/install)
* [Terraform Install](https://learn.hashicorp.com/tutorials/terraform/install-cli)

## See also

- [Terraform GitLab Provider](https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs)

## License

MIT