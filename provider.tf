terraform {
  required_version = ">= 0.13"
}

terraform {
  required_providers {
    gitlab = {
      source  = "gitlabhq/gitlab"
      version = "~> 3.6.0"
    }
  }
}

provider "gitlab" {
  token = var.gitlab_token
}