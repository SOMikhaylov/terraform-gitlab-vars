# TERRAFORM-GITLAB-VARS

[EN version](../README.md)

Данный проект позволяет автоматизировать процесс добавления `gitlab` переменных необходимых для работы `terraform` c `google cloud`.

Для корректной работы `terraform` проекта необходим файл с `google application credentials` и `terraform.tfvars`. Оба эти файла пробрасываются c помощью `terraform` в `CI/CD` переменные `gitlab` проекта.
Файл с `google application credentials` передается в закодированном формате для более безопасной передачи по сети.

---

## Использование

### 1. Получение google application credentials

```
gcloud auth application-default login
```

### 2. Настройка файла terraform.tfvars

необходимо заполнить файл terraform.tfvars в соответсвии с вашими настройками

```
cp terraform.tfvars.example terraform.tfvars
```

### 3. Запуск
Для запуска, необходимо выполнить данные команды

```
terraform init 
terraform plan
terraform apply -auto-approve 
```

### 4. Запуск для нескольких проектов

Для настройки нескольких `gitlab `проектов, рекомендуется использовать `terraform workspace`

```
terraform init 
terraform workspace new <project-name>
terraform plan -var-file -var-file=<project-terraform-tfvars> 
terraform apply -auto-approve -var-file -var-file=<project-terraform-tfvars> 
```

### 5. Удаление
```
terraform destroy -auto-approve 
```

---

## Используемые инструменты

 * terraform
 * google-cloud-sdk

---

## Установка

вы можете установить все данные инструменты с помощью данных гайдов 

* [Google Cloud sdk](https://cloud.google.com/sdk/docs/install)
* [Terraform Install](https://learn.hashicorp.com/tutorials/terraform/install-cli)


## Смотрите также

- [Terraform Google Cloud Platform Provider](https://registry.terraform.io/providers/hashicorp/google/latest/docs)

## Лицензия

MIT