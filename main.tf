resource "gitlab_project_variable" "gcp_creds" {
  project       = var.project_id
  key           = "gcp_creds"
  value         = filebase64(var.gcp_creds)
  variable_type = "file"
}

resource "gitlab_project_variable" "terraform_tfvars" {
  project       = var.project_id
  key           = "terraform_tfvars"
  value         = file(var.tfvars_path)
  variable_type = "file"
}