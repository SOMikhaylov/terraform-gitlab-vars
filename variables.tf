variable "gitlab_token" {
  description = "This is the GitLab personal access token"
  type        = string
}

variable "project_id" {
  description = "gitlab project id"
  type        = number
}

variable "gcp_creds" {
  description = "Either the path to or the contents of a service account key file in JSON format"
  type        = string
}

variable "tfvars_path" {
  description = "path to terraform.tfvars"
  type        = string
}